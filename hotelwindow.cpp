#include <QDebug>
#include <QPainter>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintPreviewDialog>
#include <QStringListModel>

#include "employeesadddialog.h"
#include "guestssearchdialog.h"
#include "findcleanerdialog.h"
#include "hotelwindow.h"
#include "ui_hotelwindow.h"

#define STATUSBAR_TIMEOUT   5000
#define HOTEL_DB_PATH       "hotel.db"

HotelWindow::HotelWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::HotelWindow)
{
    ui->setupUi(this);

    initializeTableView(ui->guests_view);
    initializeTableView(ui->employees_view);
    initializeTableView(ui->rooms_view);
}

HotelWindow::~HotelWindow()
{
    delete ui;
}

void HotelWindow::initializeTables()
{
    ui->guests_view->setModel(m_db.getGuestsModel());
    ui->guests_view->hideColumn(0);
    m_guests_filter = {};
    updateGuestsTable(nullptr);

    ui->employees_view->setModel(m_db.getEmployeesModel());
    ui->employees_view->hideColumn(0);

    ui->rooms_view->setModel(m_db.getRoomsModel());
}

void HotelWindow::initializeTableView(QTableView* table)
{
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table->setContextMenuPolicy(Qt::CustomContextMenu);
}

void HotelWindow::initializePermissions()
{
    if (m_user_group == HotelDB::MANAGER) {
        ui->db_tab->removeTab(ui->db_tab->indexOf(ui->employees_tab));
    } else if (m_user_group == HotelDB::EMPLOYEE) {
        ui->db_tab->removeTab(ui->db_tab->indexOf(ui->guests_tab));
        ui->db_tab->removeTab(ui->db_tab->indexOf(ui->rooms_tab));
        ui->employee_add_pb->hide();
    }
}

int HotelWindow::getSelectedRow(const QTableView* table)
{
    auto* selection = table->selectionModel();

    if (!selection->hasSelection()) {
        qDebug() << "no selection";
        return -1;
    }

    return selection->currentIndex().siblingAtColumn(0).data().toInt();
}

void HotelWindow::updateGuestsTable(const HotelDB::guests_filter_t* filter)
{
    m_db.updateGuestsModel(filter);
    ui->rooms_count_lb->setText(QString::number(m_db.getFreeRoomsCount()));
}

void HotelWindow::on_login_pb_clicked()
{
    qDebug() << "try to login";

    QString login = ui->login_le->text();
    QString password = ui->password_le->text();

    if (m_user_group = m_db.open(HOTEL_DB_PATH, login, password); m_user_group == -1) {
        ui->statusbar->showMessage("Ошибка входа!", STATUSBAR_TIMEOUT);
        return;
    }

    initializeTables();
    initializePermissions();
    ui->main_stack->setCurrentWidget(ui->db_page);
}

void HotelWindow::on_guests_search_pb_clicked()
{
    GuestsSearchDialog search_dialog(&m_guests_filter);

    if (search_dialog.exec() == QDialog::Accepted) {
        qDebug() << "guests search: accepted";
        updateGuestsTable(&m_guests_filter);
    }
}

void HotelWindow::on_guests_reset_pb_clicked()
{
    updateGuestsTable(nullptr);
    m_guests_filter = {};
}

void HotelWindow::on_guests_add_pb_clicked()
{
    HotelDB::guests_filter_t guest_values{};
    GuestsSearchDialog add_dialog(&guest_values);

    if (add_dialog.exec() == QDialog::Accepted && m_db.addGuest(guest_values)) {
        updateGuestsTable(nullptr);
    }
}

void HotelWindow::on_guests_view_customContextMenuRequested(const QPoint& pos)
{
    QMenu* menu = new QMenu(this);
    QAction* remove_guest = new QAction(tr("Удалить"), this);
    QAction* find_cleaner = new QAction(tr("Найти уборщика"), this);
    QAction* print_bill = new QAction(tr("Получить счет"), this);

    connect(remove_guest, &QAction::triggered, this,
            &HotelWindow::on_guests_view_removeGuest);
    connect(find_cleaner, &QAction::triggered, this,
            &HotelWindow::on_guests_view_findCleaner);
    connect(print_bill, &QAction::triggered, this,
            &HotelWindow::on_guests_view_printBill);

    menu->addAction(remove_guest);
    menu->addAction(find_cleaner);
    menu->addAction(print_bill);
    menu->exec(ui->guests_view->viewport()->mapToGlobal(pos));
}

void HotelWindow::on_guests_view_removeGuest()
{
    if (!m_db.removeGuest(getSelectedRow(ui->guests_view))) {
        ui->statusbar->showMessage("Не удалось удалить запись", STATUSBAR_TIMEOUT);
        return;
    }

    updateGuestsTable(nullptr);
}

void HotelWindow::on_guests_view_findCleaner()
{
    auto* selection = ui->guests_view->selectionModel();
    QModelIndex index = selection->currentIndex();
    int guest_id = getSelectedRow(ui->guests_view);
    QStringListModel weekdays(m_db.getWeekdays());
    QStringList guest_name;

    FindCleanerDialog::SearchFunc find_cleaner = [this, guest_id](int weekday_idx) {
        return m_db.findCleaner(guest_id, weekday_idx + 1);
    };

    for (int i{1}; i < 4; i++) {
        guest_name << index.siblingAtColumn(i).data().toString();
    }

    FindCleanerDialog dialog(guest_name.join(' '), &weekdays, find_cleaner);

    dialog.exec();
}

void HotelWindow::on_guests_view_printBill()
{
    QPrinter printer(QPrinter::HighResolution);
    QPrintPreviewDialog print_dialog(&printer);

    connect(&print_dialog, &QPrintPreviewDialog::paintRequested, this,
            &HotelWindow::on_print_bill_paintRequest);

    print_dialog.exec();
}

void HotelWindow::on_print_bill_paintRequest(QPrinter* printer)
{
    QPainter painter;

    if (!painter.begin(printer)) {
        qDebug() << "failed to open file";
        return;
    }

    HotelDB::hotel_bill_t bill;

    if (!m_db.getBill(getSelectedRow(ui->guests_view), bill)) {
        qDebug() << "failed to get bill";
        return;
    }

    QRect r = painter.viewport();

    painter.setPen(Qt::black);
    painter.setFont(QFont("Times", 14));
    painter.drawText(r, Qt::AlignHCenter, tr("Счет за проживание\n"));

    QStringList bill_rows = {
        tr("\n\nФИО: ") + bill.acronym,
        tr("Номер комнаты: ") + QString::number(bill.room),
        tr("Дата заселения: ") + bill.date,
        tr("Количество дней проживания: ") + QString::number(bill.duration),
        tr("Счет: ") + QString::number(bill.bill)
    };

    painter.setFont(QFont("Times", 12));
    painter.drawText(r, bill_rows.join('\n'));
}

void HotelWindow::on_employee_add_pb_clicked()
{
    HotelDB::employees_filter_t employee{};
    QStringListModel floors(m_db.getFloors());
    QStringListModel weekdays(m_db.getWeekdays());
    EmployeesAddDialog add_dialog(&employee, &floors, &weekdays);

    if (add_dialog.exec() == QDialog::Accepted) {
        ++employee.floor;
        ++employee.weekday;

        if (m_db.addEmployee(employee)) {
            m_db.updateEmployeesModel();
        }
    }
}

void HotelWindow::on_employees_view_customContextMenuRequested(const QPoint& pos)
{
    QMenu* menu = new QMenu(this);
    QAction* remove_employee = new QAction(tr("Удалить"), this);

    connect(remove_employee, &QAction::triggered, this,
            &HotelWindow::on_employees_view_removeEmployee);

    if (m_user_group < HotelDB::MANAGER) {
        menu->addAction(remove_employee);
    }

    menu->exec(ui->employees_view->viewport()->mapToGlobal(pos));
}

void HotelWindow::on_employees_view_removeEmployee()
{
    if (!m_db.removeEmployee(getSelectedRow(ui->employees_view))) {
        ui->statusbar->showMessage("Не удалось удалить запись", STATUSBAR_TIMEOUT);
        return;
    }

    m_db.updateEmployeesModel();
}
