#include "hoteldb.h"

#include "employeesadddialog.h"
#include "ui_employeesdialog.h"

EmployeesAddDialog::EmployeesAddDialog(HotelDB::employees_filter_t* employee,
                                       QAbstractItemModel* floor,
                                       QAbstractItemModel* weekdays, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::EmployeesDialog)
    , m_employee(employee)
{
    ui->setupUi(this);
    ui->floor_cb->setModel(floor);
    ui->weekday_cb->setModel(weekdays);
    ui->action_pb->setText("&Добавить");
}

EmployeesAddDialog::~EmployeesAddDialog()
{
    delete ui;
}

void EmployeesAddDialog::on_action_pb_clicked()
{
    m_employee->surname = ui->surname_le->text();
    m_employee->firstname = ui->firstname_le->text();
    m_employee->patronym = ui->patronym_le->text();
    m_employee->floor = ui->floor_cb->currentIndex();
    m_employee->weekday = ui->weekday_cb->currentIndex();

    if (!m_employee->surname.isEmpty() && !m_employee->firstname.isEmpty() &&
        !m_employee->patronym.isEmpty()) {
        accept();
    }
}
