#ifndef HOTELDB_H
#define HOTELDB_H

#include <QObject>
#include <memory>

#define SQL_INVALID_TEXT     QString()
#define SQL_INVALID_INT      -1

class QAbstractItemModel;

namespace HotelDB {
enum Groups {
    ADMIN = 1,
    MANAGER,
    EMPLOYEE
};

struct hotel_bill_t {
    QString acronym;
    int room{};
    QString date;
    int duration{};
    int bill{};
};

struct guests_filter_t {
    QString firstname{SQL_INVALID_TEXT};
    QString surname{SQL_INVALID_TEXT};
    QString patronym{SQL_INVALID_TEXT};
    QString passport{SQL_INVALID_TEXT};
    QString city{SQL_INVALID_TEXT};
    QString arrival_date{SQL_INVALID_TEXT};
    int duration{SQL_INVALID_INT};
    int room_number{SQL_INVALID_INT};
};

struct employees_filter_t {
    QString firstname{SQL_INVALID_TEXT};
    QString surname{SQL_INVALID_TEXT};
    QString patronym{SQL_INVALID_TEXT};
    int floor{SQL_INVALID_INT};
    int weekday{SQL_INVALID_INT};
};

class HotelDB : public QObject
{
    Q_OBJECT
public:
    explicit HotelDB(QObject *parent = nullptr);
    ~HotelDB();

    int open(QString db_path, QString login, QString password);
    void close();

    QAbstractItemModel* getGuestsModel();
    bool updateGuestsModel(const guests_filter_t* filter);
    bool addGuest(const guests_filter_t& guest_values);
    bool removeGuest(int id);
    QString findCleaner(int guest_id, int weekday_id);

    QAbstractItemModel* getEmployeesModel();
    bool updateEmployeesModel();
    bool addEmployee(const employees_filter_t& employee);
    bool removeEmployee(int id);

    QAbstractItemModel* getRoomsModel();

    QStringList getWeekdays();
    QStringList getFloors();
    int getFreeRoomsCount();

    bool getBill(int guest_id, hotel_bill_t& bill);

private:
    class HotelDBImpl;

    QStringList getList(QString column, QString table);
    bool removeRecord(QString table, QString column, int column_value);
    int getMaxID(QString table);
    bool validateUser(QString login, QString password);

    std::unique_ptr<HotelDBImpl> m_pimpl;
};
} // namespace HotelDB

#endif // HOTELDB_H
