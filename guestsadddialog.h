#ifndef GUESTSADDDIALOG_H
#define GUESTSADDDIALOG_H

#include <QDialog>

namespace Ui {
class GuestsDialog;
}

namespace HotelDB {
struct guests_filter_t;
}

class GuestsAddDialog : public QDialog
{
    Q_OBJECT

public:
    GuestsAddDialog(HotelDB::guests_filter_t* guests_filter,
                       QWidget *parent = nullptr);
    ~GuestsAddDialog();

private:
    Ui::GuestsDialog* ui{nullptr};
    HotelDB::guests_filter_t* m_guests_fiter{nullptr};

public slots:
    void on_action_pb_clicked();
};

#endif // GUESTSADDDIALOG_H
