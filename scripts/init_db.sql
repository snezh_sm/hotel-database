DROP TABLE IF EXISTS guests_rooms;
DROP TABLE IF EXISTS room_types;
DROP TABLE IF EXISTS rooms;
DROP TABLE IF EXISTS floors;
DROP TABLE IF EXISTS guests;
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS weekday;
DROP TABLE IF EXISTS timetable;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS users;

CREATE TABLE room_types(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    num_of_rooms INTEGER NOT NULL,
    cost INTEGER NOT NULL,
    description VARCHAR(50) NOT NULL
);
CREATE TABLE floors(
    floor INTEGER NOT NULL PRIMARY KEY
);
CREATE TABLE rooms (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    number INTEGER NOT NULL,
    room_types_id INTEGER NOT NULL,
    phone VARCHAR(11) NOT NULL,
    floor INTEGER NOT NULL,
    FOREIGN KEY (room_types_id) REFERENCES room_types (id),
    FOREIGN KEY (floor) REFERENCES floors (floor)
);
CREATE TABLE guests (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    surname VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    patronym VARCHAR(255),
    passport VARCHAR(255) NOT NULL,
    city VARCHAR(255),
    arrival_date DATE NOT NULL,
    duration INTEGER NOT NULL
);
CREATE TABLE guests_rooms (
    guests_id INTEGER NOT NULL,
    rooms_id INTEGER NOT NULL,
    FOREIGN KEY (guests_id) REFERENCES guests (id),
    FOREIGN KEY (rooms_id) REFERENCES rooms (id)
);
CREATE TABLE employees(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    surname VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    patronym VARCHAR(255)
);
CREATE TABLE weekday(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description VARCHAR(20) NOT NULL
);
CREATE TABLE timetable (
    floor INTEGER NOT NULL,
    employees_id INTEGER NOT NULL,
    weekday_id INTEGER NOT NULL,
    FOREIGN KEY (employees_id) REFERENCES employees (id),
    FOREIGN KEY (floor) REFERENCES floors (floor),
    PRIMARY KEY (floor, weekday_id)
);
CREATE TABLE groups (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description VARCHAR(255) NOT NULL
);
CREATE TABLE users(
    name VARCHAR(255) NOT NULL PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    groups_id INTEGER NOT NULL,
    FOREIGN KEY (groups_id) REFERENCES groups (id)
);
