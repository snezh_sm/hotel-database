#include <QRegExpValidator>

#include "hoteldb.h"

#include "guestsadddialog.h"
#include "ui_guestsdialog.h"

GuestsAddDialog::GuestsAddDialog(HotelDB::guests_filter_t* guests_filter,
                                 QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::GuestsDialog)
    , m_guests_fiter(guests_filter)
{
    ui->setupUi(this);

    QRegExpValidator* only_digits = new QRegExpValidator(QRegExp("[0-9]*"), this);

    ui->duration_le->setValidator(only_digits);
    ui->room_number_le->setValidator(only_digits);
    ui->action_pb->setText("&Добавить");

    setWindowTitle("Добавление посетителя");
    setFixedSize(size());
}

GuestsAddDialog::~GuestsAddDialog()
{
    delete ui;
}

void GuestsAddDialog::on_action_pb_clicked()
{
    bool duration_ok{false};
    bool room_number_ok{false};

    m_guests_fiter->surname = ui->surname_le->text();
    m_guests_fiter->firstname = ui->firstname_le->text();
    m_guests_fiter->patronym = ui->patronym_le->text();
    m_guests_fiter->passport = ui->passport_le->text();
    m_guests_fiter->city = ui->city_le->text();
    m_guests_fiter->arrival_date = ui->arrival_date_le->text();
    m_guests_fiter->duration = ui->duration_le->text().toInt(&duration_ok);
    m_guests_fiter->room_number = ui->room_number_le->text().toInt(&room_number_ok);

    if (!m_guests_fiter->surname.isEmpty() && !m_guests_fiter->firstname.isEmpty() &&
        !m_guests_fiter->patronym.isEmpty() && !m_guests_fiter->passport.isEmpty() &&
        !m_guests_fiter->city.isEmpty() && !m_guests_fiter->arrival_date.isEmpty() &&
        !duration_ok && !room_number_ok) {
        accept();
    }
}
