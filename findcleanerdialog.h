#ifndef FINDCLEANERDIALOG_H
#define FINDCLEANERDIALOG_H

#include <functional>
#include <QDialog>

namespace Ui {
class FindCleanerDialog;
}

class QAbstractItemModel;

class FindCleanerDialog : public QDialog
{
    Q_OBJECT

public:
    using SearchFunc = std::function<QString(int)>;

    FindCleanerDialog(QString guest, QAbstractItemModel* weekdays,
                      SearchFunc search, QWidget *parent = nullptr);
    ~FindCleanerDialog();

private:
    Ui::FindCleanerDialog *ui;
    SearchFunc m_search_func;

public slots:
    void on_search_pb_clicked();
};

#endif // FINDCLEANERDIALOG_H
